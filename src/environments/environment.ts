// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyB3QBo04W4of4bdesbsCXGgu7Uu4kD-14c',
    authDomain: 'controle-ifte.firebaseapp.com',
    databaseURL: 'https://controle-ifte.firebaseio.com',
    projectId: 'controle-ifte',
    storageBucket: 'controle-ifte.appspot.com',
    messagingSenderId: '34408792987',
    appId: '1:34408792987:web:57118b1b371c7af2a3573e',
    measurementId: 'G-95RGPRLKV6'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
