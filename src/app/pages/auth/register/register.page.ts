import { LoginService } from './../../auth/service/login.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  loginForm: FormGroup;

  constructor(
    private builder: FormBuilder,
    private service: LoginService
  ) { }
  

  ngOnInit() {
    this.loginForm = this.builder.group({
      nome: ['', [Validators.minLength(2), Validators.maxLength(19), Validators.required]],
      snome: ['', [Validators.minLength(2), Validators.maxLength(19), Validators.required]],
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.minLength(8), Validators.required]],
      confirm_pass: ['', [Validators.minLength(8), Validators.required]],
    });
  }

  createUser(){
    const user = this.loginForm.value;
    return this.service.createUser(user);
  }

}
