import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TopoLoginComponent} from './Component/topo-login/topo-login.component';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { RegisterPage } from './register/register.page';
import { ForgotPage } from './forgot/forgot.page';
import { LoginPage } from './login/login.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: '', children: [
    {path: '', component: LoginPage},
    {path: 'forgot', component: ForgotPage},
    {path: 'register', component: RegisterPage}
  ]}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations:[
    LoginPage,
    ForgotPage,
    RegisterPage,
    TopoLoginComponent
  ]
})
export class AuthRoutingModule { }
