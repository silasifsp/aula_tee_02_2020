import { LoginService } from '../../auth/service/login.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'forgot',
  templateUrl: './forgot.page.html',
  styleUrls: ['./forgot.page.scss'],
})
export class ForgotPage implements OnInit {
  loginForm : FormGroup;

  constructor(
    private builder: FormBuilder,
    private service: LoginService
  ) { }

  ngOnInit() {
    
    this.loginForm = this.builder.group({
      email: ['', [Validators.email, Validators.required]]
    });
  }


  recoverPass(){
    const email = this.loginForm.get('email').value;
    return this.service.recoverPass(email);
  }

}
