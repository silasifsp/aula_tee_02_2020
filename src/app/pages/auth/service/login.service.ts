import { User }  from 'firebase';
import { Observable } from 'rxjs';
import { NavController, ToastController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  isLoggedIn: Observable<User>
  
  constructor(
    private nav: NavController,
    private auth: AngularFireAuth,
    private toast: ToastController) 
    {
      
      this.isLoggedIn = this.auth.authState;
    }
    
    
  login(user){

    this.auth.signInWithEmailAndPassword(user.email, user.password).
    then(() => this.nav.navigateForward('home')).catch(() => this.showError());

  }


  private async showError(){
    const toast = await this.toast.create({
      message: 'E-mail ou senha inválido',
      duration:2000
    });
    
    return toast.present()
  }
  
  
  createUser(user){ 
    this.auth.createUserWithEmailAndPassword(user.email, user.password).
    then(credentials => console.log(credentials));
    this.nav.navigateBack('auth');
  }
  
  recoverPass(email){
    this.auth.sendPasswordResetEmail(email).
    then(() => this.nav.navigateBack('auth')).
    catch(error => {
      console.log(error+" Email:"+email);
    });
  }

  logout(){
    this.auth.signOut().then(() => this.nav.navigateBack('auth'));
  }
}
