import { NavController } from '@ionic/angular';
import { LoginService } from '../service/login.service'
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm : FormGroup;
  constructor(
    private builder: FormBuilder,
    private service: LoginService,
    private nav: NavController
  ) { }
    
  ngOnInit() {

    this.isUserLoggedIn();
    this.loginForm = this.builder.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.minLength(8), Validators.required]]
    });
  }

  isUserLoggedIn(){
    this.service.isLoggedIn.subscribe(user =>{
      if(user){
          //usuario logado
          console.log(user);
          this.nav.navigateForward('home');
      }else{
          
      }});
  }


  login(){
    const user = this.loginForm.value;
    this.service.login(user);
  }
}
