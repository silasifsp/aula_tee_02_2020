import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PayPage } from './../accounts/pay/pay.page';
import { RegistrationPage } from './registration/registration.page';
import { ReportPage } from './report/report.page';
import { ReceivePage } from './receive/receive.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ListaPageModule } from './lista/lista.module';
import { ListaPage } from './lista/lista.page';

const routes: Routes = [
  {
    path: '', children: [
      {path: 'pay', component: ListaPage},
      {path: 'receive', component: ListaPage},
      {path: 'report', component: ReportPage},
      {path: 'registration', component: RegistrationPage}

    ]
  },
  {
    path: 'lista',
    loadChildren: () => import('./lista/lista.module').then( m => m.ListaPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule],
  declarations: [
    PayPage,
    ReceivePage,
    RegistrationPage,
    ListaPageModule
  ]
})
export class AccountsRoutingModule { }
