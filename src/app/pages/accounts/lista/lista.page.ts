import { AlertController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { AccountService } from '../service/account.service';
import { Router } from '@angular/router';
import { on } from 'process';

@Component({
  selector: 'lista',
  templateUrl: './lista.page.html',
  styleUrls: ['./lista.page.scss'],
})
export class ListaPage implements OnInit {

  listAccounts;
  tipo;
  
  constructor(
    private account: AccountService,
    private alert: AlertController,
    private router: Router
  
  ) { }

  
  ngOnInit() {
    const url = this.router.url;
    this.tipo = url.split('/')[2];
    
    this.account.getList(this.tipo).subscribe(x => this.listAccounts = x);

    if(this.tipo == 'pay'){
      this.tipo = 'Pagar'
    }else{
      this.tipo = 'Receber'
    }
  }



  async remove(account){
    const confirm = await this.alert.create({
      header: 'Remover Conta',
      message: 'Deseja realmente remover essa conta?',
      buttons: [{
        text: 'Cancelar',
        role: 'cancel'
      },{
        text: 'Deletar',
        handler: () => this.account.remove(account)
      }]
      
    })
    confirm.present();
  }
  
  async editAccount(account){
    
    const confirm = await this.alert.create({
      header: 'Editar Conta',
      inputs: [
                {
                  name: 'parceiro',
                  value: account.parceiro,
                  placeholder: 'Parceiro Comercial'
                },
                {
                  name: 'descricao',
                  value: account.descricao,
                  placeholder: 'Descrição'
                },
                {
                  //por padrão o tipo é text
                  name: 'valor',
                  value: account.valor,
                  type: 'number',
                  placeholder: 'Valor'
                  
                }
              ],
      buttons: [{
        text: 'Cancelar',
        role: 'cancel'
      },{
        text: 'Editar',
        handler: (data) => {
            //Sintaxe de espalhamento
            const obj = {...account, ...data}
            this.account.editAccount(obj);
          }
      }]
      
    })
    confirm.present();
  }

}
