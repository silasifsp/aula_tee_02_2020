import { DateHelper } from './../../helpers/DateHelper';
import { Validators } from '@angular/forms';
import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  collection: AngularFirestoreCollection;

  constructor(

    private db: AngularFirestore
  ) { }

  registerAccount(account) {
    account.id = this.db.createId();
    this.collection = this.db.collection('account');
    return this.collection.doc(account.id).set(account);
  }

  getList(type: string) {
    this.collection = this.db.collection('account', ref => ref.where('tipo', '==', type));
    return this.collection.valueChanges();
  }

  remove(account) {
    this.collection = this.db.collection('account');
    this.collection.doc(account.id).delete();
  }

  editAccount(account) {

    this.collection = this.db.collection('account');

    this.collection.doc(account.id).update(account);
  }

  /**
   * Totaliza contas de acordo com seu tipo
   * @param type: string - modalidade de Contas
   * 
   */

  total(type, date) {
    const aux = DateHelper.breakDate(date);
    console.log(aux)
    const ano = aux.ano
    const mes = aux.mes
    const dia = aux.dia

    this.collection = this.db.collection('account', ref => ref.where('tipo', '==', type)
      .where('mes', '==', mes)
      .where('ano', '==', ano));
    return this.collection.get().pipe(map(snap => {

      let cont = 0;
      let sum = 0;

      snap.docs.map(doc => {

        const conta = doc.data();
        const valor = parseFloat(conta.valor);
        sum += valor;
        cont++;
      });

      return { num: cont, valor: sum };
    }));
  }

}
