import { AccountService } from './../service/account.service';
import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateHelper } from './../../helpers/DateHelper'
@Component({
  selector: 'registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {

  accountForm: FormGroup;
  
  constructor(
    private builder: FormBuilder,
    private nav: NavController,
    private account: AccountService
  ) {}
  ngOnInit() {
    this.initForm();
  }

  private initForm(){
    this.accountForm = this.builder.group({
      valor: ['', [Validators.required, Validators.min(0.01)]],
      data: [[new Date().toISOString(), Validators.required]],
      parceiro: ['', [Validators.required, Validators.minLength(5)]],
      descricao: ['', [Validators.required, Validators.minLength(6)]],
      tipo: ['', [Validators.required]]
      
    })
  }
  

  /*
  * Salva a nova conta no Firebase.
  */
  registerAccount(){
    let account = this.accountForm.value;
    const date = this.accountForm.get('data').value
    account = {...account, ...DateHelper.breakDate(date)};
    delete account.data;
    this.account.registerAccount(account).then(() => this.nav.navigateForward('/accounts/pay'));
  }



}
