import { AccountService } from './../accounts/service/account.service';
import { LoginService } from './../auth/service/login.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  date = new Date().toISOString();

  conta={
      pagar:{
        valor: 0,
        num:0
      },
      receber: {
        valor: 0,
        num:0
      },
      saldo:{
        valor:0,
        num:0
      }
    };
  
    constructor(
    private service: LoginService,
    private contas: AccountService
  ) {}

  ionViewWillEnter(){
    this.atualizaContas();
  }
  
  atualizaContas(){
    this.contas.total('pay', this.date).subscribe(
      (x:any) => {
        this.conta.pagar = x;
        
        this.contas.total('receive',this.date).subscribe(
          (y: any) => {
            this.conta.receber = y
            this.atualizarSaldo();
          }
        )
      })
    }

  atualizarSaldo(){
    this.conta.saldo.num = this.conta.pagar.num + this.conta.receber.num;
    this.conta.saldo.valor = this.conta.receber.valor - this.conta.pagar.valor
  }

  logout(){
    this.service.logout();
  }

}
